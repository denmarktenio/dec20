CREATE DATABASE stockInventory;
USE stockInventory;

CREATE TABLE fruitType(
    id BIGINT NOT NULL AUTO_INCREMENT,
    typeName VARCHAR(255),
    PRIMARY KEY (id)
);

--Data
INSERT INTO fruitType (typeName)
VALUES ("Citrus"), ("Stone Fruit"), ("Tropical Fruit"), ("Berries"), ("Melon"), ("Others");


CREATE TABLE fruitProducts(
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    quantity INT,
    fruitTypeId BIGINT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (fruitTypeId) REFERENCES fruitType(id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Lemon", 2, 1);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Lemon", 2, 1);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Grapefruit", 4, 1);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Orange", 6, 1);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Peach", 8, 2);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Plum", 10, 2);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Cherry", 12, 2);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Dragon Fruit", 14, 3);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Passion Fruit", 16, 3);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Jackfruit", 18, 3);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Blueberry", 20, 4);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Raspberry", 22, 4);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Cranberry", 18, 4);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Watermelon", 16, 5);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Snap Melon", 18, 5);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Cantaloupe", 18, 5);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Mango", 18, 6);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Apple", 16, 6);
INSERT INTO fruitProducts (name, quantity, fruitTypeId)
VALUES ("Banana", 18, 6);



CREATE TABLE userTypes(
    id BIGINT NOT NULL AUTO_INCREMENT,
    type VARCHAR(255),
    PRIMARY KEY (id)
);


INSERT INTO userTypes (type) VALUES ("Admin"), ("Staff");

CREATE TABLE users(
    id BIGINT NOT NULL AUTO_INCREMENT,
    username VARCHAR(255),
    password VARCHAR(255),
    userTypeId BIGINT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (userTypeId) REFERENCES userTypes(id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

INSERT INTO users (username, password, userTypeId) VALUES ("adminUsername", "adminPassword", 1);
INSERT INTO users (username, password, userTypeId) VALUES ("admin2", "admin2", 1);
INSERT INTO users (username, password, userTypeId) VALUES ("admin3", "admin3", 1);
INSERT INTO users (username, password, userTypeId) VALUES ("admin4", "admin4", 1);
INSERT INTO users (username, password, userTypeId) VALUES ("admin5", "admin5", 1);
INSERT INTO users (username, password, userTypeId) VALUES ("admin6", "admin6", 1);
INSERT INTO users (username, password, userTypeId) VALUES ("staffUsername", "staffPassword", 2);
INSERT INTO users (username, password, userTypeId) VALUES ("staff2", "staff2", 2);
INSERT INTO users (username, password, userTypeId) VALUES ("staff3", "staff3", 2);
INSERT INTO users (username, password, userTypeId) VALUES ("staff4", "staff4", 2);
INSERT INTO users (username, password, userTypeId) VALUES ("staff5", "staff5", 2);
INSERT INTO users (username, password, userTypeId) VALUES ("staff6", "staff", 2);



------------------------------------------------------------------------------------QUERIES----------------------------------------------------------------------=
UPDATE userTypes
SET id = 3
WHERE type = "Admin";

UPDATE fruitType
SET id = 9
WHERE typeName = "Others";

SELECT COUNT(*) AS totalNumberOfUsers FROM users;

SELECT AVG(quantity) AS "averageFruit?" FROM fruitProducts;

SELECT SUM(quantity) AS "totalNumberOfFruits" FROM fruitProducts;

SELECT name AS "fewestCitrusFruit", MIN(quantity) FROM fruitProducts WHERE fruitTypeId = 1;

SELECT name AS "mostMelonFruit", MAX(quantity) FROM fruitProducts WHERE fruitTypeId = 5;